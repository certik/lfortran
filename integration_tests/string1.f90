program print_my_name
implicit none

character(len = 7) :: my_name = 'Dominic'
   
print *, 'My name is ', my_name
   
end program print_my_name
